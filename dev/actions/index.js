export const createContact = contact => ({
    type: 'CREATE_CONTACT',
    payload: contact,
});

export const updateContact = contact => ({
    type: 'UPDATE_CONTACT',
    payload: contact,
});

export const removeContact = id => ({
    type: 'REMOVE_CONTACT',
    payload: id,
});