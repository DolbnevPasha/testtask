import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';	

import Title from './components/Title';
import List from './components/List';
import Form from './components/Form';

import store from './store';

import styles from './style.css';


const App = () => (
	<Provider store={store}>
		<div className={styles.box}>								 
			<Title />
			<List />
			<Form 
				type="create"
				contentForButton="Post"
			/>
		</div>
	</Provider>
)

ReactDOM.render(
	<App />,
	document.getElementById('app')
);