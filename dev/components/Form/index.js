import React, { useState } from 'react';
import { useDispatch } from 'react-redux'; 

import { updateContact, createContact } from '../../actions';

import Button from '../Button';

import styles from './style.css';

const Form = ({item = {}, type, hideForm, contentForButton}) => {
    const [name, setName] = useState(item.name || '');
    const [phone, setPnone] = useState(item.phone || '');
    const dispatch = useDispatch();
    const update = (item) => dispatch(updateContact(item));
    const create = (item) => dispatch(createContact(item));

    const handleChange = (e) => {
        const { name, value } = e.target;

        switch (name) {
            case 'name':
                setName(value);
                break;
            case 'phone':
                setPnone(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!name.trim() || !phone.trim()) {
            return
        }

        const data = {                           
            id: item.id || Date.now(),     
            name: name,                        
            phone: phone,                          
        };

        switch (type) {
            case 'create':
                create(data);
                break;
            case 'update':
                update(data);
                hideForm();
                break;
            default:
                break;
        }

        setName('');
        setPnone('');
    }

    return (
        <form
            className={`${styles.form} ${type == 'update' ? styles.update : ''}`}
            onSubmit={handleSubmit}
        >
            <input
                className={styles.input}
                type="text"
                id="name"
                name="name"
                placeholder="Name"
                value={name}
                onChange={handleChange}
            />
            <input
                className={styles.input}
                type="number"
                name="phone"
                id="phone"
                placeholder="Phone"
                value={phone}
                onChange={handleChange}
            />
            <Button
                theme="main"
            >
                {contentForButton}
            </Button>
        </form>
    );
}

export default Form;