import React from 'react';

import styles from './style.css';

const Title = () => (
	<h1 className={styles.title}>Телефонный справочник</h1>
);

export default Title;