import React, { useState } from 'react';

import Button from '../Button';
import Icon from '../Icon';
import Form from '../Form';

import styles from './style.css';

const Item = ({item}) => {
    const [isShowForm, handleShowForm] = useState(false);

    const handleChange = () => {
        handleShowForm(!isShowForm);
    }

    return (
        <>
            {
                isShowForm ? (
                    <Form
                        type="update"
                        item={item}
                        hideForm={handleChange}
                        contentForButton="Edit"
                    />
                ) : 
			    <li className={styles.item}>
			        <p className={styles.field}>{item.name}</p>
			        <p className={styles.field}>{item.phone}</p>
				    <Button
                        type="editContact"
                        theme="small"
                        handleClick={handleChange}
                    >
                        <Icon name="edit" />
                    </Button>
				    <Button
                        type="deleteContact"
                        theme="small"
                        itemId={item.id}
                    >
                        <Icon name="delete" />
                    </Button>
			    </li>  
            }
        </>  
    );
}

export default Item;