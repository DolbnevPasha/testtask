import React from 'react';
import { useDispatch } from 'react-redux'; 

import { removeContact } from '../../actions';

import styles from './style.css';

const Button = ({type, handleClick, itemId, theme, children}) => {
    const dispatch = useDispatch();
    const remove = (itemId) => dispatch(removeContact(itemId));

    const handleChange = () => {
        switch (type) {            
            case 'editContact':
                handleClick();
                break;                
            case 'deleteContact':
                remove(itemId);
                break;                
            default:
                break;
        }
    }

    const getClassName = () => {
        switch (theme) {
            case 'main':
			    return styles.main;
            case 'small':
                return styles.small;
		    default:
			    break;
        }
    }

    return (
        <button
            className={getClassName()}
            onClick={handleChange}
        >
            {children}
        </button>
    );
}

export default Button;