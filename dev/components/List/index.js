import React from 'react';
import { useSelector } from 'react-redux';

import Item from '../Item';

import styles from './style.css';

const List = () => {
	const contacts = useSelector(store => store);
	return (
		<>
			{
            	contacts.length ? (
                	<ul>
						{
							contacts.map(item => (
								<Item
				  					key={item.id}
				  					item={item}
								/>
							))
						}
					</ul>
            	) : 
				<p className={styles.text}>Список пуст</p>
			}
		</>
	);
};

export default List;