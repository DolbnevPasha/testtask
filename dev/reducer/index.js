const defaultStore = JSON.parse(localStorage.getItem('Contacts')) || [];

const reducer = (store=defaultStore, { type, payload }) => {
	let contacts = store;
	switch(type) {
		case 'CREATE_CONTACT': {
			contacts = [...store, payload];
			localStorage.setItem('Contacts', JSON.stringify(contacts));
			return contacts;
		}
		case 'UPDATE_CONTACT': {
			contacts = contacts.map(item => item.id === payload.id ? payload : item);
			localStorage.setItem('Contacts', JSON.stringify(contacts));
			return contacts;
		}
		case 'REMOVE_CONTACT': {
			contacts = contacts.filter(item => item.id !== payload);
			localStorage.setItem('Contacts', JSON.stringify(contacts));
			return contacts;
		}
		default: {
			return contacts;
		}
	}
};

export default reducer;